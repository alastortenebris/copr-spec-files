%global debug_package %{nil}

Name:           7-zip
Version:        21.07
Release:        1%{?dist}
Summary:        7-Zip is a file archiver for Windows.

License:        LGPLv2
URL:            https://www.7-zip.org
Source0:        https://www.7-zip.org/a/7z2107-src.tar.xz
# Patch0:         fix_cflags.patch

BuildRequires:  gcc-c++
BuildRequires:  make

%description


%prep
%setup -c
# %patch0 -p1

%build

export DISABLE_RAR=1
cd CPP/7zip/Bundles/Alone2/
%make_build -f ../../cmpl_gcc.mak

%install
mkdir -p %{buildroot}%{_bindir}
install -m 755 CPP/7zip/Bundles/Alone2/b/g/7zz %{buildroot}%{_bindir}/7zz


%files
%license DOC/copying.txt
%{_bindir}/7zz

%changelog
* Sun Feb 13 2022 Alastor Tenebris
- 
