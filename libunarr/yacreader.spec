%global appname YACReader
%global versuf 2301161

Name:           yacreader
Version:        9.11.0.%{versuf}
Release:        1%{?dist}
Summary:        Cross platform comic reader and library manager

# The entire source code is GPLv3+ except:
# BSD:          QsLog
#               folder_model
# MIT:          pictureflow
License:        GPLv3+ and BSD and MIT
URL:            https://www.yacreader.com
Source0:        https://github.com/YACReader/%{name}-dev-builds/releases/download/%{version}/%{name}-%{version}-src.tar.xz

BuildRequires:  desktop-file-utils
BuildRequires:  gcc-c++
BuildRequires:  make
BuildRequires:  pkgconfig(glu)
BuildRequires:  systemd-rpm-macros

BuildRequires:  pkgconfig(libarchive)
BuildRequires:  pkgconfig(poppler-qt5)
BuildRequires:  pkgconfig(Qt5)
BuildRequires:  pkgconfig(Qt5Multimedia)
BuildRequires:  pkgconfig(Qt5ScriptTools)
BuildRequires:  pkgconfig(Qt5Svg)
BuildRequires:  pkgconfig(Qt5QuickControls2)
BuildRequires:  dos2unix
BuildRequires:  qt5-qtimageformats-devel
# For YACReaderLibrary QR Code display
BuildRequires:  pkgconfig(libqrencode)

Requires:       hicolor-icon-theme
Requires:       qt5-qtgraphicaleffects%{?_isa}
Requires:       qt5-qtquickcontrols%{?_isa}

%description
Best comic reader and comic manager with support for .cbr .cbz .zip .rar comic
files.


%prep
%autosetup -n %{name}-%{version} -p1

# wrong-file-end-of-line-encoding fix
dos2unix INSTALL.md

# file-not-utf8 fix
iconv -f iso8859-1 -t utf-8 README.md > README.md.conv && mv -f README.md.conv README.md


%build
%qmake_qt5 CONFIG+=libarchive
%make_build


%install
%make_install INSTALL_ROOT=%{buildroot}
%find_lang %{name} --with-qt
%find_lang %{name}library --with-qt


%check
desktop-file-validate %{buildroot}%{_datadir}/applications/*.desktop


%files -f %{name}.lang -f %{name}library.lang
%license COPYING.txt
%doc CHANGELOG.md README.md INSTALL.md
%{_bindir}/%{appname}
%{_bindir}/%{appname}Library
%{_bindir}/%{appname}LibraryServer
%{_datadir}/%{name}/
%{_datadir}/applications/*.desktop
%{_datadir}/icons/hicolor/*/*/*.svg
%{_mandir}/man1/*.1*
%{_userunitdir}/*.service


%changelog
* Sat Jan 22 2022 Fedora Release Engineering <releng@fedoraproject.org> - 9.8.2-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_36_Mass_Rebuild

* Fri Nov 26 2021 Artem Polishchuk <ego.cordatus@gmail.com> - 9.8.2-1
- chore(update): 9.8.2 | Co-authored-by: Jack Xu <jackyzy823@gmail.com>
- build(add BR): libqrencode
- build(add BR): Qt5Svg

* Fri Jul 23 2021 Fedora Release Engineering <releng@fedoraproject.org> - 9.7.1-4
- Rebuilt for https://fedoraproject.org/wiki/Fedora_35_Mass_Rebuild

* Tue Mar 30 2021 Jonathan Wakely <jwakely@redhat.com> - 9.7.1-3
- Rebuilt for removed libstdc++ symbol (#1937698)

* Thu Jan 28 2021 Fedora Release Engineering <releng@fedoraproject.org> - 9.7.1-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_34_Mass_Rebuild

* Sat Sep 12 2020 Artem Polishchuk <ego.cordatus@gmail.com> - 9.7.1-1
- Update to 9.7.1

* Fri Sep  4 2020 Artem Polishchuk <ego.cordatus@gmail.com> - 9.7.0-1
- Update to 9.7.0

* Wed Jul 29 2020 Fedora Release Engineering <releng@fedoraproject.org> - 9.6.2-4
- Rebuilt for https://fedoraproject.org/wiki/Fedora_33_Mass_Rebuild

* Sun Feb 02 2020 Artem Polishchuk <ego.cordatus@gmail.com> - 9.6.2-3
- Add dep: qt5-qtgraphicaleffects
- Add dep: qt5-qtquickcontrols

* Fri Jan 31 2020 Fedora Release Engineering <releng@fedoraproject.org> - 9.6.2-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_32_Mass_Rebuild

* Tue Jan 21 2020 Artem Polishchuk <ego.cordatus@gmail.com> - 9.6.2-1
- Update to 9.6.2
- Enable LTO

* Fri Jan 17 2020 Marek Kasik <mkasik@redhat.com> - 9.6.0-2
- Rebuild for poppler-0.84.0

* Mon Sep 16 2019 Artem Polishchuk <ego.cordatus@gmail.com> - 9.6.0-1
- Update to 9.6.0

* Sat Jul 27 2019 Fedora Release Engineering <releng@fedoraproject.org> - 9.5.0-7
- Rebuilt for https://fedoraproject.org/wiki/Fedora_31_Mass_Rebuild

* Fri Mar 29 2019 Artem Polishchuk <ego.cordatus@gmail.com> - 9.5.0-6
- Initial package.
