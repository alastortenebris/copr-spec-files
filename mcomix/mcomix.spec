Name:		mcomix
Version:	3.1.0
Release:	1%{?dist}
Summary:	User-friendly, customizable image viewer for comic books

URL:		http://mcomix.sourceforge.net/
# Version info: mcomix/mcomixstarter.py
License:	GPLv2+
Source0:	https://downloads.sourceforge.net/%{name}/%{name}-%{version}.tar.gz

BuildArch:      noarch
BuildRequires:  pkgconfig(python3)
Requires:       hicolor-icon-theme
BuildRequires:  desktop-file-utils
BuildRequires:  libappstream-glib
Recommends:     p7zip-plugins
Recommends:     mupdf

%description
MComix is a user-friendly, customizable image viewer. It is specifically
designed to handle comic books, but also serves as a generic viewer. It
reads images in ZIP, RAR, 7Zip or tar archives as well as plain image files. It
is written in Python and uses GTK+ through the PyGObject bindings.

%prep
%autosetup -p1
%generate_buildrequires
%pyproject_buildrequires -r

%build
%pyproject_wheel

%install
%pyproject_install
cp -pr share %{buildroot}%{_prefix}

desktop-file-validate %{buildroot}%{_datadir}/applications/%{name}.desktop
appstream-util validate-relax --nonet %{buildroot}%{_metainfodir}/%{name}.metainfo.xml


rm %{buildroot}%{_datadir}/icons/hicolor/*/mimetypes/*.png


%files
%license COPYING
%{_bindir}/%{name}
%{python3_sitelib}/%{name}-%{version}.dist-info/
%{python3_sitelib}/%{name}
%{_mandir}/man1/%{name}.*
# Do not own %%{_datadir}/icons/hicolor explicitly
%{_datadir}/icons/hicolor/*/apps/%{name}.*
%{_metainfodir}/%{name}.metainfo.xml
%{_datadir}/applications/%{name}.desktop
%{_datadir}/mime/packages/%{name}.xml


%changelog
%autochangelog
