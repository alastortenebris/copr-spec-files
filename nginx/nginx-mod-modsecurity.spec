%global origname modsecurity-nginx

Name:           nginx-mod-modsecurity
Version:        1.0.3 
Release:        1%{?dist}
Summary:        nginx module which supports purging ngx_http_(fastcgi|proxy|scgi|uwsgi)_module caches
License:        BSD
URL:            https://github.com/SpiderLabs/ModSecurity-nginx
Source0:        https://github.com/SpiderLabs/ModSecurity-nginx/releases/download/v%{version}/%{origname}-v%{version}.tar.gz
BuildRequires:  gcc-c++
BuildRequires:  nginx-mod-devel
BuildRequires:  pkgconfig(modsecurity)

%description
ngx_cache_purge is an nginx module which adds the ability to purge content from the FastCGI, proxy, SCGI and uWSGI caches.

%prep
%autosetup -n %{origname}-v%{version}

%build
%nginx_modconfigure
%nginx_modbuild

%install
pushd %{_vpath_builddir}
install -dm 0755 %{buildroot}%{nginx_moddir}
install -pm 0755 ngx_http_modsecurity_module.so %{buildroot}%{nginx_moddir}
install -dm 0755 %{buildroot}%{nginx_modconfdir}
echo 'load_module "%{nginx_moddir}/ngx_http_modsecurity_module.so";' \
    > %{buildroot}%{nginx_modconfdir}/mod-modsecurity.conf
popd

%files
%license LICENSE
%doc README.md
%{nginx_moddir}/ngx_http_modsecurity_module.so
%{nginx_modconfdir}/mod-modsecurity.conf

%changelog
%autochangelog
