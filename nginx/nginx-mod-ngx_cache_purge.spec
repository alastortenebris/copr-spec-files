Name:           nginx-mod-ngx_cache_purge
Version:        2.5.2
Release:        1%{?dist}
Summary:        nginx module which supports purging ngx_http_(fastcgi|proxy|scgi|uwsgi)_module caches
License:        BSD
URL:            https://github.com/nginx-modules/ngx_cache_purge
Source0:        https://github.com/nginx-modules/ngx_cache_purge/archive/refs/tags/%{version}.tar.gz#/%{name}-%{version}.tar.gz
BuildRequires:  gcc-c++
BuildRequires:  nginx-mod-devel

%description
ngx_cache_purge is an nginx module which adds the ability to purge content from the FastCGI, proxy, SCGI and uWSGI caches.

%prep
%autosetup -n ngx_cache_purge-%{version}

%build
%nginx_modconfigure
%nginx_modbuild

%install
pushd %{_vpath_builddir}
install -dm 0755 %{buildroot}%{nginx_moddir}
install -pm 0755 ngx_http_cache_purge_module.so %{buildroot}%{nginx_moddir}
install -dm 0755 %{buildroot}%{nginx_modconfdir}
echo 'load_module "%{nginx_moddir}/ngx_http_cache_purge_module.so";' \
    > %{buildroot}%{nginx_modconfdir}/mod-http_cache_purge.conf
popd

%files
%license LICENSE
%doc README.md
%{nginx_moddir}/ngx_http_cache_purge_module.so
%{nginx_modconfdir}/mod-http_cache_purge.conf

%changelog
%autochangelog