%global srcnm Qalculate
%global libversion 22
%global libsymlink 16.1

Summary:	Multi-purpose calculator library
Name:		libqalculate
Version:	4.1.0
Release:	1%{?dist}
License:	GPLv2+

URL:		https://qalculate.github.io/
Source0:	https://github.com/%{srcnm}/%{name}/releases/download/v%{version}/%{name}-%{version}.tar.gz

BuildRequires:	gcc-c++
BuildRequires:	pkgconfig(glib-2.0)
BuildRequires:	pkgconfig(cln)
BuildRequires:	intltool
BuildRequires:	pkgconfig(libxml-2.0)
BuildRequires:	pkgconfig(readline)
BuildRequires:	pkgconfig(ncurses)
BuildRequires:	pkgconfig(libcurl)
BuildRequires:	libicu-devel
BuildRequires:	pkgconfig(mpfr)
BuildRequires:	perl(XML::Parser)
BuildRequires:	gettext
BuildRequires:	perl(Getopt::Long)
BuildRequires:  make
BuildRequires:  libtool

%description
This library underpins the Qalculate! multi-purpose desktop calculator for
GNU/Linux

%package	devel
Summary:	Development tools for the Qalculate calculator library
Requires:	%{name}%{?_isa} = %{version}-%{release}
Requires:	pkgconfig(glib-2.0)
Requires:	pkgconfig(libxml-2.0)
Requires:	pkgconfig(cln)
Requires:	pkgconfig(mpfr)

%description	devel
The libqalculate-devel package contains the header files needed for development
with libqalculate.

%package -n	qalculate
Summary:	Multi-purpose calculator, text mode interface
Requires:	%{name}%{?_isa} = %{version}-%{release}
Requires:	pkgconfig

Provides:	qalc

%description -n	qalculate
Qalculate! is a multi-purpose desktop calculator for GNU/Linux. It is
small and simple to use but with much power and versatility underneath.
Features include customizable functions, units, arbitrary precision, plotting.
This package provides the text-mode interface for Qalculate! The GTK and QT
frontends are provided by qalculate-gtk and qalculate-kde packages resp.

%prep
%setup -q

%build
%configure --disable-static --disable-rpath

%make_build

%install
%make_install

%find_lang %{name}
rm -f %{buildroot}/%{_libdir}/*.la

%ldconfig_scriptlets

%files -f %{name}.lang
%doc AUTHORS ChangeLog TODO
%license COPYING
%{_libdir}/libqalculate.so.%{libversion}
%{_libdir}/libqalculate.so.%{libversion}.%{libsymlink}
%{_datadir}/qalculate/
%{_mandir}/man1/qalc.1.*

%files devel
%{_libdir}/libqalculate.so
%{_libdir}/pkgconfig/libqalculate.pc
%{_includedir}/libqalculate/

%files -n qalculate
%{_bindir}/qalc

%changelog
%autochangelog
