Summary:	A multi-purpose desktop calculator for GNU/Linux
Name:		qalculate-qt
Version:	4.1.0
Release:	1%{?dist}

License:	GPLv2+
URL:		http://qalculate.sourceforge.net/
Source0:	https://github.com/Qalculate/%{name}/releases/download/v%{version}/%{name}-%{version}.tar.gz

BuildRequires:  make
BuildRequires:  gcc-c++
BuildRequires:  desktop-file-utils
BuildRequires:  gettext
BuildRequires:  pkgconfig(Qt5Gui)
BuildRequires:  pkgconfig(Qt5UiPlugin)
BuildRequires:  pkgconfig(libqalculate)
BuildRequires:	libappstream-glib

%description
Qalculate! is a modern multi-purpose desktop calculator for GNU/Linux. It is
small and simple to use but with much power and versatility underneath.
Features include customizable functions, units, arbitrary precision, plotting.

This package provides a QT5 graphical interface for Qalculate!

%prep
%autosetup

%build
%qmake_qt5 PREFIX=%{_prefix}
%make_build

%install
%make_install INSTALL_ROOT=%{buildroot}

%check
appstream-util validate-relax --nonet %{buildroot}%{_metainfodir}/%{name}.appdata.xml
desktop-file-validate %{buildroot}/%{_datadir}/applications/%{name}.desktop

%files
%doc AUTHORS README
%license COPYING
%{_bindir}/%{name}
%{_datadir}/%{name}/
%{_datadir}/applications/%{name}.desktop
%{_metainfodir}/%{name}.appdata.xml
%{_mandir}/man1/%{name}.1*
%{_datadir}/icons/hicolor/*/*/qalculate*


%changelog
* Fri Jan 21 2022 Fedora Release Engineering <releng@fedoraproject.org> - 0.9.7.10-33.nmu1
- Rebuilt for https://fedoraproject.org/wiki/Fedora_36_Mass_Rebuild
